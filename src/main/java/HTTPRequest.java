import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class HTTPRequest {

  public File getResponceToYandex(File path) throws IOException {
    String USER_AGENT =
        "user-agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36";
    String url = "https://yandex.ru/maps";
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url);
    request.addHeader("User-Agent", USER_AGENT);
    HttpResponse response = client.execute(request);
    PrintWriter out = new PrintWriter(path.getAbsoluteFile());
    BufferedReader rd =
        new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    String line = "";
    Header[] headers = response.getAllHeaders();
    for (int i = 0; i < headers.length; i++) {
      line = line + headers[i].getValue() + "\n";
    }
    out.print(line);
    while ((line = rd.readLine()) != null) {
      out.print(line);
    }
    rd.close();
    out.close();
    return path;
  }

  public String getCsrfToken(File input) {
    String temp = "";
    BufferedReader in;
    try {
      in = new BufferedReader(new FileReader(input.getAbsoluteFile()));
      String line = "";
      while ((line = in.readLine()) != null) {
        temp = temp + line;
      }
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Pattern p = Pattern.compile("\\\"(csrfToken)\\\"\\:\\\"([a-z0-9]{1,}\\:[a-z0-9]{1,})\\\"");
    Matcher m = p.matcher(temp);
    String tmp = "";
    while (m.find()) {
      tmp = m.group(1) + "=" + m.group(2);
    }
    return tmp;
  }

  public String getYandexuid(File input) {
    String temp = "";
    BufferedReader in;
    try {
      in = new BufferedReader(new FileReader(input.getAbsoluteFile()));
      String line = "";
      while ((line = in.readLine()) != null) {
        temp = temp + line;
      }
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Pattern p = Pattern.compile("(yandexuid\\=([0-9]{1,}))\\;");
    Matcher m = p.matcher(temp);
    String tmp = "";
    while (m.find()) {
      tmp = m.group(1);
    }
    return tmp;

  }


  public String getCoordinate(File input, String search) {
    String url = "https://yandex.ru/maps/api/search?text="+search+"&lang=ru_RU&"
        + getCsrfToken(input);
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url.replaceAll(" ", "%20"));
    request.addHeader("Cookie", getYandexuid(input));
    try {
      HttpResponse response = client.execute(request);
      String body = EntityUtils.toString(response.getEntity());
      Pattern p = Pattern.compile("(InternalToponymInfo\\\"\\:\\{.*?\\})");
      Matcher m = p.matcher(body);
      while (m.find()) {
        return m.group(1);
      }

    } catch (UnsupportedOperationException | IOException e) {
      e.printStackTrace();
    }
    return null;
  }


}
